# vvv custom site template
Template de site customizado para ser utilizado no desenvolvimento de temas do Wordpress utilizando o Varying Vagrant Vagrants.

## Visão geral
Este template permitirá que você crie um ambiente de desenvolvimento do WordPress usando apenas o vvv-custom.yml.

Os ambientes suportados são:
- Um único site
- Um subdomínio multisite
- Um subdiretório multisite

## Configuração

### As configurações mínimas exigidas são:

```
seusite:
  repo: https://gitlab.com/metas360/vvv-custom-site-template.git
  hosts:
    - seusite.local
```
| Configuração | Valor          |
|--------------|----------------|
| Domínio      | seusite.local  |
| WP Título    | seusite.local  |
| WP Tipo      | single         |
| WP Versão    | latest         |
| DB Nome      | seusite        |

## Opções de configuração

```
hosts:
    - seusite.local
    - blog.seusite.local
    - outrosite.local
```
Define os domínios e hosts para o VVV escutar.
O primeiro domínio na lista é o domínio principal de seu sites.

```
custom:
    site_title: Meu Incrível Website
```
Define o título do site a ser definido na instalação do WordPress.

```
custom:
    wp_version: 4.9.8
```
Define a versão do WordPress que você deseja instalar.
Os valores válidos são:
- nightly
- latest
- um número de versão específica

Versões mais antigas do WordPress não serão executadas no PHP7.
Para maiores informações, visite a página de documentação do Varying Vagrant Vagrants para saber [como mudar a versão do PHP](https://varyingvagrantvagrants.org/docs/en-US/adding-a-new-site/changing-php-version/).

```
custom:
    wp_type: single
```
Define o tipo de instalação do WordPress que você está criando.
Os valores válidos são:
- single
- subdomain
- subdirectory

```
custom:
    db_name: super_secet_db_name
```

Define o nome do banco de dados para a instalação.

## Patrocinadores

Ajude-nos apoiando nossos esforços de desenvolvimento de código aberto [tornando-se um patrão](https://www.patreon.com/metas360).